
-- SUMMARY --

This module allows you to easily and quickly integrate the GiroPay payment method
into your website with an Ubercart online shop.

Before you can make use of GiroPay you need to sign up with a PSP and an Acquirer.
This module is designed to work with http://www.girosolutions.de, a platform of a
PSP called Tineon AG.

More details about GiroPay can be found on http://www.giropay.de.

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure GiroPay in Administer >> Store Administration >> Config Store >> Payment

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* J�rgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de

