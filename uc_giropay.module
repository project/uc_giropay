<?php

/**
 * @file
 * The main file for the uc_giropay module.
 *
 * 
 * 
 */

include_once('uc_giropay.inc');
include_once('uc_giropay.pages.inc');

/**
 * Implementation of hook_help().
 */
function uc_giropay_help($path, $arg) {
  switch ($path) {
    case 'admin/store/uc_giropay':
      return '<p>'. t('Here you can set up parameters for !name.', array('!name' => _uc_giropay_name(),)) .'</p>';
    case 'admin/help#uc_giropay':
      return filter_filter('process', 2, NULL, file_get_contents(dirname(__FILE__) ."/README.txt"));
  }
}

/**
 * Implementation of hook_menu().
 */
function uc_giropay_menu() {
  $items = array();
  $access_config = array('administer site configuration');
  $access_content = array('access content');
  $items[_uc_giropay_path_post('payment_link_redirect')] = array(
    'title' => t('Payment with !name completed successfully', array('!name' => _uc_giropay_name(),)),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_giropay_payment_redirect'),
    'access arguments' => $access_content,
    'type' => MENU_CALLBACK,
    'file' => 'uc_giropay.pages.inc',
  );
  $items[_uc_giropay_path_post('payment_link_notify')] = array(
    'title' => 'Payment http notify link',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_giropay_payment_notify'),
    'access arguments' => $access_content,
    'type' => MENU_CALLBACK,
    'file' => 'uc_giropay.pages.inc',
  );
  return $items;
}

/**
 * Internal default variables for template_var().
 */
function uc_giropay_variables() {
  return array(
    'uc_giropay_merchant_id' => '',
    'uc_giropay_project_id' => '',
    'uc_giropay_passphrase' => '',
  );
}

/**
 * Internal implementation of variable_get().
 */
function uc_giropay_var($name, $default = NULL) {
  static $defaults = NULL;
  if (!isset($defaults)) {
    $defaults = uc_giropay_variables();
  }
  $name = 'uc_giropay_'. $name;
  return variable_get($name, isset($default) || !isset($defaults[$name]) ? $default : $defaults[$name]);
}

/**
 * Implementation of hook_order().
 */
function uc_giropay_order($op, &$arg1, $arg2) {
  if ($arg1->payment_method == 'GiroPay') {
    switch ($op) {
      case 'submit':
      case 'save':
        _uc_giropay_save_bankcode_to_order($arg1->payment_details['uc_giropay_bankcode'], $arg1->order_id);
        break;
      case 'load':
        $arg1->payment_details['uc_giropay_bankcode'] = _uc_giropay_load_bankcode_from_order($arg1->order_id);
        break;
    }
  }
}

/**
 * Implementation of hook_payment_method().
 */
function uc_giropay_payment_method() {
  $methods[] = array(
    'id' => 'GiroPay',
    'name' => _uc_giropay_name(),
    'title' => _uc_giropay_name_logo(),
    'review' => _uc_giropay_description(),
    'desc' => _uc_giropay_description(),
    'callback' => 'uc_giropay_setting',
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );
  return $methods;
}

/*
 * Add GiroPay to the payment method settings form
 */
function uc_giropay_setting($op, &$arg1) {
  switch ($op) {
    case 'cart-details':
      $details = drupal_get_form('uc_giropay_bankcode_form', $arg1);
      return uc_strip_form($details);
    case 'cart-process':
      // Go ahead and put the bankcode in the payment details array.
      $arg1->payment_details = array('uc_giropay_bankcode' => check_plain($_POST['uc_giropay_bankcode']));
      return TRUE;
    case 'cart-review':
      if (isset($arg1->payment_details['uc_giropay_bankcode']) && !empty($arg1->payment_details['uc_giropay_bankcode'])) {
        $review[] = array('title' => t('Bankcode of your bank'), 'data' => check_plain($arg1->payment_details['uc_giropay_bankcode']));
      }
      return $review;
    case 'settings':
      $form = array();
      $form['uc_giropay_merchant_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant ID'),
        '#default_value' => uc_giropay_var('merchant_id'),
      );
      $form['uc_giropay_project_id'] = array(
        '#type' => 'textfield',
        '#title' => t('Project ID'),
        '#default_value' => uc_giropay_var('project_id'),
      );
      $form['uc_giropay_passphrase'] = array(
        '#type' => 'textfield',
        '#title' => t('Project passphrase'),
        '#default_value' => uc_giropay_var('passphrase'),
      );
      $form['notice'] = array(
        '#value' => '<div>'. t('To signup with !name and/or create a new project for payments go to the !url.', array('!name' => _uc_giropay_name(), '!url' => l(_uc_giropay_name(). ' website', _uc_giropay_path_post('create_project_form')))) .'</div>',
      );
      return $form;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function uc_giropay_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);
    if ($order->payment_method == 'GiroPay') {
      unset($form['submit']);
      $form['#prefix'] = '<table style="display: inline; padding-top: 1em;"><tr><td>';
      $form['#suffix'] = '</td><td>'. drupal_get_form('uc_giropay_checkout_form', $order) .'</td></tr></table>';
    }
  }
}

// Displays the bankcode form on the checkout screen.
function uc_giropay_bankcode_form($form_state, $order) {
  $form = array();
  $form['uc_giropay_bankcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Bankleitzahl'),
    '#default_value' => '',
    '#description' => t('Bitte geben Sie die BLZ Ihrer Bank ein, von der aus Sie den Betrag überweisen möchten, damit wir Sie direkt auf das Homebanking-Portal Ihrer Bank weiterleiten können. Sie können dieses Feld auch leer lassen und die Bank zu einem späteren Zeitpunkt auswählen.'),
  );
  return $form;
}

// Saves a bankcode to an order's data array.
function _uc_giropay_save_bankcode_to_order($bankcode, $order_id) {
  // Load up the existing data array.
  $data = db_result(db_query("SELECT data FROM {uc_orders} WHERE order_id = %d", $order_id));
  $data = unserialize($data);

  // Stuff the serialized and encrypted CC details into the array.
  $data['uc_giropay_bankcode'] = $bankcode;

  // Save it again.
  db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($data), $order_id);
}

// Load a bankcode from an order's data array.
function _uc_giropay_load_bankcode_from_order($order_id) {
  // Load up the existing data array.
  $data = db_result(db_query("SELECT data FROM {uc_orders} WHERE order_id = %d", $order_id));
  $data = unserialize($data);
  return $data['uc_giropay_bankcode'];
}
