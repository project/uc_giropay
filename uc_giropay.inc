<?php

/**
 * @file
 * Parameter file for the uc_giropay module.
 *
 * In this file all system parameters from GiroSolutions are collected and
 * get returned to the module according to the current context.
 */

define('UC_GIROPAY_PSP_HOST', 'payment.girosolutions.de');
define('UC_GIROPAY_WEBSITE', 'http://www.girosolutions.de');

/**
 *
 */
function _uc_giropay_url_base() {
  return 'https://'. UC_GIROPAY_PSP_HOST;
}

/**
 *
 */
function _uc_giropay_url_post($type) {
  global $base_url;
  $url = _uc_giropay_url_base();
  switch ($type) {
    case 'start_payment':
      return $url .'/payment/start';
    default:
      return $base_url .'/'. _uc_giropay_path_post($type);
  }
}

/**
 *
 */
function _uc_giropay_path_post($type) {
  switch ($type) {
    case 'create_project_form':
      return UC_GIROPAY_WEBSITE;
    case 'payment_link_redirect':
      return 'cart/uc_giropay/payment/redirect';
    case 'payment_link_notify':
      return 'cart/uc_giropay/payment/notify';
  }
}

/**
order_id=41
gpCode=1900
gpHash=7c4bb3b22a06686ad4ef21a62490f5bd
 *
 */
function _uc_giropay_check_hash() {
  $data = array(
    'merchantId' => uc_giropay_var('merchant_id'),
    'projectId' => uc_giropay_var('project_id'),
    'transactionId' => check_plain($_GET['order_id']),
    'gpCode' => check_plain($_GET['gpCode']),
  );
  $hashdata = array(
    $data['merchantId'],
    $data['projectId'],
    $data['transactionId'],
    $data['gpCode'],
  );
  $hash_check = check_plain($_GET['gpHash']);
  $hash = _uc_giropay_get_hash($hashdata);
  if ($hash == $hash_check) {
    $data['verified'] = TRUE;
  }
  else {
    $data['verified'] = FALSE;
  }
  return $data;
}

/**
 * 
 */
function _uc_giropay_get_hash($hashdata) {
  $hashdata_implode = implode('', $hashdata);
  return hash_hmac('md5', $hashdata_implode, uc_giropay_var('passphrase'));
}

/**
 * 
 */
function _uc_giropay_name() {
  return 'GiroPay';
}

/**
 * 
 */
function _uc_giropay_logo() {
  $logo  = drupal_get_path('module', 'uc_giropay') .'/ressources/logo.jpg';
  $alt   = _uc_giropay_name();
  $title = t('Payment method !name.', array('!name' => _uc_giropay_name(),));
  return theme_image($logo, $alt, $title, array(), FALSE);
}

/**
 * 
 */
function _uc_giropay_name_logo() {
  return _uc_giropay_logo();
}

/**
 * 
 */
function _uc_giropay_submit() {
  return t('Submit Order');
}

/**
 *
 */
function _uc_giropay_description() {
  $desc = t('Mit giropay zahlen Sie im Internet einfach, schnell und sicher per Online-Überweisung. Das Online-Zahlverfahren giropay wurde innerhalb der deutschen Kreditwirtschaft entwickelt und hat mit der Postbank, den Sparkassen und den Volksbanken Raiffeisenbanken starke vertrauensvolle Partner, die höchste Sicherheitsstandards garantieren.');
  return _uc_giropay_logo() .'<div>'. $desc .'</div>';
}

/**
 * Liefert zu einem Giropay Fehlercode (gpCode) die 
 * deutsche Fehlerbeschreibung.
 *
 * @param integer $gpCode Fehlercode von Giropay
 * @return string Fehlerbeschreibung
 */
function _uc_giropay_getCodeDescription($gpCode) {
  switch ($gpCode) {
    case 1900:
      return t('Fehler bei Transaktionsstart. Dei übergebenen Daten sind unbrauchbar.');
    case 1910:
      return t('Fehler/Abbruch bei Transaktionsstart. BLZ ungültig oder BLZ-Suche abgebrochen.');
    case 3100:
      return t('Benutzerseitiger Abbruch bei der Bezahlung.');
    case 4000:
      return t('Bezahlung erfolgreich.');
    case 4500:
      return t('Unbekanntes Transaktionsende. Zahlungseingang muss anhand der Kontoumsätze überprüft werden.');
    case 4900:
      return t('Bezahlung nicht erfolgreich.');
    default:
      return t('Unbekannter Fehler');
  }
}

/**
 * Liefert einen Text, der dem Benutzer nach der Bezahlung über Giropay angezeigt wird
 *
 * @param integer $gpCode Fehler Code
 * @return string Meldung für den Benutzer
 */
function _uc_giropay_getCodeText($gpCode) {
	switch ($gpCode) {
	  case 1900:
	    return t('Fehler bei der Bezahlung über Giropay. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 1910:
	    return t('Fehler bei der Bezahlung über Giropay. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 3100:
	    return t('Sie haben die Bezahlung über Giropay abgebrochen. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 4000:
	    return t('Vielen Dank für die Bezahlung über Giropay.');
	  case 4500:
	    return t('Es ist ein unbekannter Fehler bei der Bezahlung über Giropay aufgetreten. Bitte überprüfen Sie Ihre Kontoumsätze und wenden Sie sich bei Fragen an uns.');
	  case 4900:
	    return t('Fehler bei der Bezahlung über Giropay. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	}
	return t('Es ist ein unbekannter Fehler bei der Bezahlung über Giropay aufgetreten. Bitte überprüfen Sie Ihre Kontoumsätze und wenden Sie sich bei Fragen an uns.');
}

/**
 * Liefert, ob der angegebene Code OK bedeutet
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_giropay_codeIsOK($gpCode) {
	if ($gpCode == '4000') {
	  return TRUE;
  }
	return FALSE;
}

/**
 * Liefert, ob der angegebene Code ein Unbekannter ausgang ist
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_giropay_codeIsUnbekannt($gpCode) {
  if ($gpCode == '4500') {
	  return TRUE;
  }
	return FALSE;
}

/**
 * Liefert, ob der angegebene Code ein Fehler ist
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_giropay_codeIsFehler($gpCode) {
  if ($gpCode != '4000' && $gpCode != '4500') {
	  return TRUE;
  }
	return FALSE;
}
