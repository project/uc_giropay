<?php

/**
 * @file
 * Forms for the uc_giropay module.
 *
 */

/**
 *
 */
function uc_giropay_payment_redirect() {
  $data = _uc_giropay_check_hash();
  if (!$data['verified']) {
    watchdog('uc_giropay', 'Received redirect with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    return;
  }
  else {
    watchdog('uc_giropay', 'Received valid redirect with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
  }
  $order_id = $data['transactionId'];
  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }
  $gpCode = $data['gpCode'];
  if (_uc_giropay_codeIsOK($gpCode)) {
    // This lets us know it's a legitimate access of the complete page.
    $_SESSION['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    watchdog('uc_giropay', _uc_giropay_getCodeText($gpCode), array(), WATCHDOG_ERROR);
    drupal_set_message(_uc_giropay_getCodeText($gpCode), 'warning');
    drupal_goto('cart/checkout');
  }
}

/**
 *
 */
function uc_giropay_payment_notify() {
  $data = _uc_giropay_check_hash();
  $result = '400';
  if (!$data['verified']) {
    watchdog('uc_giropay', t('Received notification with invalid GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
  }
  else {
    watchdog('uc_giropay', t('Received valid notification with GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
    $order_id = $data['transactionId'];
    $order = uc_order_load($order_id);
    if ($order == FALSE) {
      watchdog('uc_giropay', 'Notification attempted for non-existent order.', array(), WATCHDOG_ERROR);
    }
    else {
      $gpCode = $data['gpCode'];
      if (_uc_giropay_codeIsOK($gpCode)) {
        $amount = check_plain($_GET['amount']);
        $context = array(
          'revision' => 'formatted-original',
          'location' => 'paypal-ipn',
        );
        $options = array(
          'sign' => FALSE,
        );
        $comment = t('GiroPay transaction');
        uc_payment_enter($order_id, 'giropay', $amount, $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through GiroPay.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')), 'order', 'payment_received');
        uc_order_comment_save($order_id, 0, t('GiroPay reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')));
        $result = '200';
      }
    }
  }
  print $result;
  exit;
}

// Returns the form elements for the GiroPay checkout form.
function uc_giropay_checkout_form($form_state, $order) {
  $context = array(
    'revision' => 'formatted-original',
    'location' => 'giropay-form',
  );
  $options = array(
    'sign' => FALSE,
    'thou' => FALSE,
    'dec' => ',',
  );
  $shipping = 0;
  foreach ($order->line_items as $item) {
    if ($item['type'] == 'shipping') {
      $shipping += $item['amount'];
    }
  }
  $tax = 0;
  if (module_exists('uc_taxes')) {
    foreach (uc_taxes_calculate($order) as $tax_item) {
      $tax += $tax_item->amount;
    }
  }
  $data = array(
    'merchantId' => uc_giropay_var('merchant_id'),
    'projectId' => uc_giropay_var('project_id'),
    'transactionId' => $order->order_id,
    'amount' => uc_price($order->order_total, $context, $options),
    'vwz' => 'Order No. '. $order->order_id .'-'. $order->uid,
    'bankcode' => $order->payment_details['uc_giropay_bankcode'],
    'urlRedirect' => _uc_giropay_url_post('payment_link_redirect') .'?order_id='. $order->order_id,
    'urlNotify' => _uc_giropay_url_post('payment_link_notify') .'?order_id='. $order->order_id .'&amount='. $order->order_total,
  );
  $hashdata = array(
    $data['merchantId'],
    $data['projectId'],
    $data['transactionId'],
    $data['amount'],
    $data['vwz'],
    $data['bankcode'],
    $data['urlRedirect'],
    $data['urlNotify'],
  );
  $data['hash'] = _uc_giropay_get_hash($hashdata);
  $form['#action'] = _uc_giropay_url_post('start_payment');
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => _uc_giropay_submit(),
  );
  return $form;
}
